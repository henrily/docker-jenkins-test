#FROM composer:1.5.2 as project
#WORKDIR /app
#COPY . .
#RUN composer install

FROM php:7.1.9-apache
#FROM debian:stretch
#COPY --from=project /app /app
COPY . /app
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

RUN rm -rf /var/www/* \
    && a2enmod rewrite\
    && echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN cp docker/vhost.conf /etc/apache2/sites-available/000-default.conf


# Add main start script for when image launches
RUN chmod 0755 docker/run.sh
RUN docker/run.sh

EXPOSE 80
CMD ["apache2-foreground"]
