#!/bin/bash

mkdir -p var/cache var/logs
touch var/logs/prod.log
chgrp -R www-data .
chmod -R g+w var/logs var/cache var/sessions var/
